01 - Introduction
02 - The MERN Stack - Theory
03 - Planning the App
04 - React.js - A Refresher
05 - React.js - Building the Frontend
06 - Node.js & Express.js - A Refresher
07 - Node.js & Express.js - Building our REST API
08 - Working with MongoDB & Mongoose - A Refresher
09 - Connecting the Backend to the Database - MongoDB & Mongoose
10 - Connecting the React.js Frontend to the Backend
11 - Adding File Upload
12 - Adding Authentication
13 - Application Deployment
14 - Roundup & Next Steps


External Links:

GraphQL-vs-REST-API
https://academind.com/learn/node-js/graphql-with-node-react-full-app/

Build-a-Complete-GraphQL-API-with-Node-
https://academind.com/learn/node-js/graphql-with-node-react-full-app/

Primitive-vs-Reference-Values
https://academind.com/learn/javascript/reference-vs-primitive-values/

Promises-vs-async-await
https://academind.com/learn/javascript/callbacks-vs-promises-vs-rxjs-vs-async-awaits/

MongoDB-Atlas
https://www.mongodb.com/cloud/atlas

Build-a-GraphQL-API-Frontend-
https://academind.com/learn/node-js/graphql-with-node-react-full-app/
 
